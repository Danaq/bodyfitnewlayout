<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div id='mobileEval'>
			<a href="http://bodyfit.onlinebedarfsanalyse.de/">Passt Bodyfit zu mir?</a>
		</div>
		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the page content template.
			get_template_part( 'template-parts/content', 'page' );
			the_content();
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}

			// End of the loop.
		endwhile;
		?>

	</main><!-- .site-main -->

	<?php get_sidebar( 'content-bottom' ); ?>
	<div id="footer-navigation">
		<div id="contactFooter">
			<h3>Fitnessstudio Bodyfit</h3>
			<?php echo do_shortcode("[impressum_manager type=\"address\"]"); ?>
			<?php echo do_shortcode("[impressum_manager type=\"contact\"]"); ?>
		</div> 
		<div id="footerLinks">
			<a id="impress" href="<?php echo get_site_url() . "/impressum/" ?>" >Impressum</a> <br />
			<a id="datapriv" href="<?php echo get_site_url() . "/datenschutz/" ?>" >Datenschutz</a> <br />
			<a id="approach" href="<?php echo get_site_url() . "/anfahrt/" ?>">Anfahrt</a> <br />
			<a id="footerContact" href="<?php echo get_site_url() . "/kontakt/" ?>">Kontakt</a> <br />
		</div>  
	</div>
</div><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
