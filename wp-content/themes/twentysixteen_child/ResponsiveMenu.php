<!--					<li><a href='".get_site_url()."/geraete/schwingungstrainer-trainieren-wie-die-stars/'>Schwingungstrainer - Trainieren wie die STARS!!!</a></li>-->
<?php
$out = "
<button id='btn'><span>MENÜ</span></button>
	<nav class='main-nav' id='main-nav'>
		<ul class='main-nav-ul'>
			<li><a href='".get_site_url()."/'>Home</a></li>
			<li class='has-sub'><a class='sub-arrow'>Studio</a>
				<ul class='subs'>
					<li><a href='".get_site_url()."/geraete/'>Geräte</a></li>
					<li><a href='".get_site_url()."/kurse-gymnastik/'>Kurse/ Gymnastik</a></li>
					<li><a href='".get_site_url()."/solarium-2/'>Solarium/ Wasserstrahlmassage</a></li>
					<li><a href='".get_site_url()."/sauna-dampfbad/'>Sauna/ Dampfbad</a></li>
					<li><a href='".get_site_url()."/magnetfeld-therapie/'>Magnetfeld-Therapie</a></li>
				</ul>
			</li>
			<li><a href='".get_site_url()."/dorn-seminar-termine-2/'>Dorn</a></li>
			<li><a href='".get_site_url()."/frauen/'>Frauen</a></li>
			<li><a href='".get_site_url()."/maenner/'>Männer</a></li>
			<li><a href='".get_site_url()."/healthy-balance/'>Ernährung</a></li>
			<li class='has-sub'><a class='subb-arrow'>Bilder</a>
				<ul class='subss'>
					<li><a href='".get_site_url()."/bilder/studiorundgang/'>Studiorundgang</a></li>
					<li><a href='".get_site_url()."/bilder/betreuung/'>Betreuung</a></li>
					<li><a href='".get_site_url()."/bilder/cardio/'>Cardio</a></li>
					<li><a href='".get_site_url()."/bilder/krafttraining/'>Krafttraining</a></li>
					<li><a href='".get_site_url()."/bilder/rueckenbilder/'>Rückentraining</a></li>
					<li><a href='".get_site_url()."/bilder/kurse/'>Kurse</a></li>
					<li><a href='".get_site_url()."/bilder/wellness/'>Wellness</a></li>
					<li><a href='".get_site_url()."/bilder/geraetebilder/'>Geräte</a></li>
					<li class='has-sub-sub'><a class='sub-sub-arrow'>Fotos</a>
						<ul class='subb'>
							<li><a class='erster-arrow'>2016</a>
								<ul class='dp1'>
									<li><a href='".get_site_url()."/bilder/events/2016-2/casino-night-2016/'>Casino Night 2016</a></li>
								</ul>
							</li>
							<li><a href='".get_site_url()."/bilder/events/2014-2/'>2014</a></li>
							<li><a class='zweiter-arrow'>2013</a>
								<ul class='dp2'>
									<li><a href='".get_site_url()."/bilder/events/2013-2/schnee-party/'>Schnee-Party</a></li>
									<li><a href='".get_site_url()."/bilder/events/2013-2/casino-night/'>Casino Night</a></li>
								</ul>
							</li>
							<li><a class='dritter-arrow'>2012</a>
								<ul class='dp3'>
									<li><a href='".get_site_url()."/bilder/events/2012-2/schuelerinnen-beim-zumba/'>Schülerinnen beim Zumba</a></li>
									<li><a href='".get_site_url()."/bilder/events/2012-2/schueler-beim-zirkeltraining/'>Schüler beim Zirkeltraining</a></li>
									<li><a href='".get_site_url()."/bilder/events/2012-2/spinning-party/'>Spinning-Party</a></li>
									<li><a href='".get_site_url()."/bilder/events/2012-2/verlosungsparty/'>Verlosungsparty</a></li>
								</ul>
							</li>
							<li><a class='vierter-arrow'>2011</a>
								<ul class='dp4'>
									<li><a href='".get_site_url()."/bilder/events/2011-2/neueroeffnungs-feier/'>Neueröffnungs-Feier</a></li>
									<li><a href='".get_site_url()."/bilder/events/2011-2/gewinnerfotos/'>Gewinnerfotos</a></li>
								</ul>
							</li>
							<li><a href='".get_site_url()."/bilder/events/2010-2/'>2010</a></li>
							<li><a class='fünfter-arrow'>Bilder von 	&amp; vor 2009</a>
								<ul class='dp5'>
									<li><a href='".get_site_url()."/bilder/events/bilder-von-vor-2009/verschiedene-aktionen/'>Verschiedene Aktionen</a></li>
									<li><a href='".get_site_url()."/bilder/events/bilder-von-vor-2009/weihnachtsfeier-2009/'>Weihnachtsfeier</a></li>
									<li><a href='".get_site_url()."/bilder/events/bilder-von-vor-2009/rafting-2009/'>Rafting</a></li>
									<li><a href='".get_site_url()."/bilder/events/bilder-von-vor-2009/grillfest-2009/'>Grillfest</a></li>
									<li><a href='".get_site_url()."/bilder/events/bilder-von-vor-2009/gesund-fit-mit-spass/'>Gesund &amp; Fit mit Spaß</a></li>
								</ul>
							</li>
						</ul>
					</li>
				</ul>
			</li>
			<li><a href='".get_site_url()."/oeffnungszeiten/'>Öffnungszeiten</a></li>
			<li><a href='".get_site_url()."/?page_id=13'>Anfahrt</a></li>
		</ul>
	</nav>
";
echo $out;
?>
