<?php
/*
Template Name: news
*/
define( "MIS_TEMPLATE_DIR", get_site_url() . "/wp-content/themes/twentysixteen_child");
?>

<?php

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<?php
			// Include the page content template.
			get_template_part( 'template-parts/content', 'page' );
		?>
		
		<?php
			query_posts('cat=6');
			if(have_posts()) {
				insertNews();
			}
		?>
			
		<?php get_sidebar( 'content-bottom' ); ?>
	</main><!-- .content-area -->
	<div id="footer-navigation">
		<div id="contactFooter">
			<h3>Fitnessstudio Bodyfit</h3>
			<?php echo do_shortcode("[impressum_manager type=\"address\"]"); ?>
			<?php echo do_shortcode("[impressum_manager type=\"contact\"]"); ?>
		</div> 
		<div id="footerLinks">
			<a id="impress" href="<?php echo get_site_url() . "/impressum/" ?>" >Impressum</a>
			<br />
			<a id="datapriv" href="<?php echo get_site_url() . "/datenschutz/" ?>" >Datenschutz</a>
			<br />
			<a id="approach" href="<?php echo get_site_url() . "/anfahrt/" ?>" >Anfahrt</a>
			<br />
			<a id="footerContact" href="<?php echo get_site_url() . "/Kontakt/" ?>" >Kontakt</a>
			<br />
		</div>  
	</div>
</div>	
<?php get_sidebar(); ?>
<?php get_footer(); ?>
<?php
function insertNews() {
	echo ("<div class='news'>");
		echo ("<h2>News</h2>");
			query_posts('posts_per_page=2', 'cat=6'); 
			while (have_posts()) : the_post();
		echo '<article class="newsElement">';
		echo ("<h3 class=\"newsTopic\">");
			the_title();
		echo ("</h3>");
			the_content();
		echo "</article>";
			endwhile;
	echo ("</div>");
}
?>	