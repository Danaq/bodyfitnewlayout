<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
define( "MIS_TEMPLATE_DIR", get_site_url() . "/wp-content/themes/twentysixteen_child");
?>

</div><!-- .site-info -->

		<footer id="colophon" class="site-footer" role="contentinfo">

			<div class="site-info">
				<?php
					/**
					 * Fires before the twentysixteen footer text for footer customization.
					 *
					 * @since Twenty Sixteen 1.0
					 */
					do_action( 'twentysixteen_credits' );
				?>
<!--
				<span class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></span>
				<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'twentysixteen' ) ); ?>"><?php printf( __( 'Proudly powered by %s', 'twentysixteen' ), 'WordPress' ); ?></a>
-->
			</div>
		</footer><!-- .site-footer -->

<?php wp_footer(); ?>

<a title="zum Angebot" href="<?php echo get_site_url() . "/angebot/" ?>" >
	<div id="button-image-offer">
		Angebot
	</div>
</a>
<div id="button-image-contact">
	Kontakt
</div>
</body>
</html>