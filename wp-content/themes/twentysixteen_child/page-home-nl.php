<?php
/*
Template Name: Home dark-grid
*/
define( "MIS_TEMPLATE_DIR", get_site_url() . "/wp-content/themes/twentysixteen_child");

$content = get_field('home_grid');

?>

<?php

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<?php
			// Include the page content template.
			get_template_part( 'template-parts/content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}
		?>
		<div id="grid_container">
			<?php
				foreach($content as $tile) { ?>

				<article id="<?php echo $tile['tile_id'] ?>" class="grid_element">
					<header>
						<h1><?php echo $tile['tile_title'] ?></h1>
					</header>
					<a href="<?php 
						if($title['tile_link'] == null) {
							echo $tile['tile_external_link'];
						} else {
							echo $tile['tile_link'];
						} 
						?>">
						<?php
						if($tile['tile_image'] != null) {
						?>	
							<img src="<?php echo $tile['tile_image']['sizes']['large'] ?>" alt="<?php echo $tile['tile_image']['alt'] ?>">
						<?php
						} 
						echo $tile['tile_content']; ?>
					</a>
				</article>
			<?php
				}	
			?>
		</div>

	</main><!-- .site-main -->
	
	<?php get_sidebar( 'content-bottom' ); ?>
	<div id="footer-navigation">
		<div id="contactFooter">
			<h3>Fitnessstudio Bodyfit</h3>
			<?php echo do_shortcode("[impressum_manager type=\"address\"]"); ?>
			<?php echo do_shortcode("[impressum_manager type=\"contact\"]"); ?>
		</div> 
		<div id="footerLinks">
			<a id="impress" href="<?php echo get_site_url() . "/impressum/" ?>" >Impressum</a>
			<br />
			<a id="datapriv" href="<?php echo get_site_url() . "/datenschutz/" ?>" >Datenschutz</a>
			<br />
			<a id="approach" href="<?php echo get_site_url() . "/anfahrt/" ?>" >Anfahrt</a>
			<br />
			<a id="footerContact" href="<?php echo get_site_url() . "/Kontakt/" ?>" >Kontakt</a>
			<br />
		</div>  
	</div>
</div><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>	