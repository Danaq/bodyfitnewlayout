cookiesc = jQuery.noConflict();	
cookiesc(document).ready(function(){

		// Cookie setting script wrapper
		var cookieScripts = function () {
			// Internal javascript called
//			console.log("Running");
		

		}
	
		/* Call cookiesDirective, overriding any default params*/
		


				
		
		
		cookiesc.cookiesDirective({
			privacyPolicyUri: 'myprivacypolicy.html',
				explicitConsent: false,
			    position : 'bottom',
				duration: 10,
				limit: 0,
				message: null,				
				cookieScripts: null,
				scriptWrapper: cookieScripts,	
			    cookieScripts: 'Google Analytics, My Stats Ultimate ',                     
				fontFamily: 'helvetica',
				fontColor: '#FFFFFF',
				fontSize: '13px',
				backgroundColor: '#666',
				backgroundOpacity: '85',
				linkColor: '#fff'

		});
	});

;(function($) {
	$.cookiesDirective = function(options) {
			
		// Default Cookies Directive Settings
		var settings = $.extend({
			//Options
			explicitConsent: true,
			position: 'top',
			duration: 10,
			limit: 0,
			message: null,				
			cookieScripts: null,
			privacyPolicyUri: 'privacy.html',
			scriptWrapper: function(){},	
			// Styling
			fontFamily: 'helvetica',
			fontColor: '#FFFFFF',
			fontSize: '13px',
			backgroundColor: '#000000',
			backgroundOpacity: '80',
			linkColor: '#CA0000'
		}, options);
		
		// Perform consent checks
		if(!getCookie('cookiesDirective')) {
			if(settings.limit > 0) {
				// Display limit in force, record the view
				if(!getCookie('cookiesDisclosureCount')) {
					setCookie('cookiesDisclosureCount',1,1);		
				} else {
					var disclosureCount = getCookie('cookiesDisclosureCount');
					disclosureCount ++;
					setCookie('cookiesDisclosureCount',disclosureCount,1);
				}
				
				// Have we reached the display limit, if not make disclosure
				if(settings.limit >= getCookie('cookiesDisclosureCount')) {
					disclosure(settings);		
				}
			} else {
				// No display limit
				disclosure(settings);
			}		
			
			// If we don't require explicit consent, load up our script wrapping function
			if(!settings.explicitConsent) {
				settings.scriptWrapper.call();
			}	
		} else {
			// Cookies accepted, load script wrapping function
			settings.scriptWrapper.call();
		}		
	};
	
	// Used to load external javascript files into the DOM
	$.cookiesDirective.loadScript = function(options) {
		var settings = $.extend({
			uri: 		'', 
			appendTo: 	'body'
		}, options);	
		
		var elementId = String(settings.appendTo);
		var sA = document.createElement("script");
		sA.src = settings.uri;
		sA.type = "text/javascript";
		sA.onload = sA.onreadystatechange = function() {
			if ((!sA.readyState || sA.readyState == "loaded" || sA.readyState == "complete")) {
				return;
			} 	
		};
		switch(settings.appendTo) {
			case 'head':			
				$('head').append(sA);
			  	break;
			case 'body':
				$('body').append(sA);
			  	break;
			default: 
				$('#' + elementId).append(sA);
		}
	};
	
	// Helper scripts
	// Get cookie
	var getCookie = function(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for(var i=0;i < ca.length;i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1,c.length);
			if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length,c.length);
		}
		return null;
	};
	
	// Set cookie
	var setCookie = function(name,value,days) {
		var expires = "";
		if (days) {
			var date = new Date();
			date.setTime(date.getTime()+(days*24*60*60*1000));
			expires = "; expires="+date.toGMTString();
		}
		document.cookie = name+"="+value+expires+"; path=/";
	};
	
	// Detect IE < 9
	var checkIE = function(){
		var version;
		if (navigator.appName == 'Microsoft Internet Explorer') {
	        var ua = navigator.userAgent;
	        var re = new RegExp("MSIE ([0-9]{1,}[\\.0-9]{0,})");
	        if (re.exec(ua) !== null) {
	            version = parseFloat(RegExp.$1);
			}	
			if (version <= 8.0) {
				return true;
			} else {
				if(version == 9.0) {
					if(document.compatMode == "BackCompat") {
						// IE9 in quirks mode won't run the script properly, set to emulate IE8	
						var mA = document.createElement("meta");
						mA.content = "IE=EmulateIE8";				
						document.getElementsByTagName('head')[0].appendChild(mA);
						return true;
					} else {
						return false;
					}
				}	
				return false;
			}		
	    } else {
			return false;
		}
	};

	// Disclosure routines
	var disclosure = function(options) {
		var settings = options;
		settings.css = 'fixed';
		
		// IE 9 and lower has issues with position:fixed, either out the box or in compatibility mode - fix that
		if(checkIE()) {
			settings.position = 'top';
			settings.css = 'absolute';
		}
		
		// Any cookie setting scripts to disclose
		var scriptsDisclosure = '';
		if (settings.cookieScripts) {
			var scripts = settings.cookieScripts.split(',');
			var scriptsCount = scripts.length;
			var scriptDisclosureTxt = '';
			if(scriptsCount>1) {
				for(var t=0; t < scriptsCount - 1; t++) {
					 scriptDisclosureTxt += scripts[t] + ', ';	
				}	
				scriptsDisclosure = ' We use ' +  scriptDisclosureTxt.substring(0,  scriptDisclosureTxt.length - 2) + ' and ' + scripts[scriptsCount - 1] + ' scripts, which all set cookies. ';
			} else {
				scriptsDisclosure = ' We use a ' + scripts[0] + ' script which sets cookies.';		
			}
		} 
		
		// Create overlay, vary the disclosure based on explicit/implied consent
		// Set our disclosure/message if one not supplied
		var html = ''; 

		html += '<div id="cookiesdirective" style="-webkit-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.25); -moz-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.25); box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.25);position:'+ settings.css +';'+ settings.position + ':-300px;left:0px;width:100%;';
		html += 'height:auto;background:' + settings.backgroundColor + ';opacity:.' + settings.backgroundOpacity + ';';
		html += '-ms-filter: “alpha(opacity=' + settings.backgroundOpacity + ')”; filter: alpha(opacity=' + settings.backgroundOpacity + ');';
		html += '-khtml-opacity: .' + settings.backgroundOpacity + '; -moz-opacity: .' + settings.backgroundOpacity + ';';
		html += 'color:' + settings.fontColor + ';font-family:' + settings.fontFamily + ';font-size:' + settings.fontSize + ';';
		html += 'text-align:left;line-height: 1.2em;padding: 10px 20px 20px;z-index:1000;">';
		html += '<div style="position:relative;height:auto;width:90%;padding:10px;margin:0px auto 10px;">';
			


		
if($("html[lang='de-DE']").length || $("html[lang='de']").length){

			// Explicit consent disclosure
			html += '<div style="width:65%; max-width: 870px; margin:0 10px 0 0; float:left;" ><strong style="font-size:14px;">Cookie Hinweis</strong>';
			html += '<p style="padding:0; margin:0;">Diese Webseite verwendet Cookies. Wenn Sie diese Webseite weiterhin besuchen, stimmen Sie der Nutzung von Cookies zu </p>';

			// Implied consent disclosure
			html += '<span>Mehr Details finden Sie unter </span><a style="color:'+ settings.linkColor + ';';
			html += 'font-weight:bold;font-family:' + settings.fontFamily + ';font-size:' + settings.fontSize + ';" href="'+ settings.privacyPolicyUri + '">Datenschutz</a>.</div>';
			html += '<input style="margin:5px 0px 0 0; cursor:pointer; background:transparent; border:2px solid #fff; -webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px; text-transform: uppercase; color:#fff; padding:10px 20px; float: left;" type="submit" name="impliedsubmit" id="impliedsubmit" value="Zur Kenntnis genommen"/>';	

}else{
			// Explicit consent disclosure
			html += '<div style="width:65%; max-width: 870px; margin:0 10px 0 0; float:left;" ><strong style="font-size:14px;">Note on Cookies</strong>';
			html += '<p style="padding:0; margin:0;">We use cookies to improve performance and enhance the user experience for those who visit our website. By continuing to use the site you are agreeing to its use of cookies. </p>';

			// Implied consent disclosure
			html += '<span>More details </span><a style="color:'+ settings.linkColor + ';';
			html += 'font-weight:bold;font-family:' + settings.fontFamily + ';font-size:' + settings.fontSize + ';" href="'+ settings.privacyPolicyUri + '">Data Privacy</a>.</div>';
			html += '<input style="margin:5px 0px 0 0; cursor:pointer; background:transparent; border:2px solid #fff; -webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px; text-transform: uppercase; color:#fff; padding:10px 20px; float: left;" type="submit" name="impliedsubmit" id="impliedsubmit" value="O.K."/>';	
}      
		// Build the rest of the disclosure for implied and explicit consent
	
		html += '</div>';
		$('body').append(html);
		
		// Serve the disclosure, and be smarter about branching
		var dp = settings.position.toLowerCase();
		if(dp != 'top' && dp!= 'bottom') {
			dp = 'top';
		}	
		var opts = { in: null, out: null};
		if(dp == 'top') {
			opts.in = {'top':'0'};
			opts.out = {'top':'-300'};
		} else {
			opts.in = {'bottom':'0'};
			opts.out = {'bottom':'-300'};
		}		

		// Start animation
		$('#cookiesdirective').animate(opts.in, 1000, function() {
			// Set event handlers depending on type of disclosure
			if(settings.explicitConsent) {
				// Explicit, need to check a box and click a button
				$('#explicitsubmit').click(function() {
					if($('#epdagree').is(':checked')) {	
						// Set a cookie to prevent this being displayed again
						setCookie('cookiesDirective',1,365);	
						// Close the overlay
						$('#cookiesdirective').animate(opts.out,1000,function() { 
							// Remove the elements from the DOM and reload page
							$('#cookiesdirective').remove();
							location.reload(true);
						});
					} else {
						// We need the box checked we want "explicit consent", display message
						$('#epdnotick').css('display', 'block'); 
					}	
				});
			} else {
				// Implied consent, just a button to close it
				$('#impliedsubmit').click(function() {
					// Set a cookie to prevent this being displayed again
					setCookie('cookiesDirective',1,365);	
					// Close the overlay
					$('#cookiesdirective').animate(opts.out,1000,function() { 
						// Remove the elements from the DOM and reload page
						$('#cookiesdirective').remove();
					});
				});
			}	
			
			if(settings.duration > 0)
			{
				// Set a timer to remove the warning after 'settings.duration' seconds
				setTimeout(function(){
					$('#cookiesdirective').animate({
						opacity:'0.8'
					},2000, function(){
						$('#cookiesdirective').css(dp,'0px');
					});
				}, settings.duration * 1000);
			}
		});	
	};
})(jQuery);
