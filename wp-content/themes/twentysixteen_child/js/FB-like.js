//	$.noConflict();
jQuery('#socialshareprivacy').socialSharePrivacy({
    services : {
        twitter : {
            'status' : 'off'
        },
        gplus : {
            'status' : 'off'
        }
    },
    'css_path'  : '/socialshareprivacy/socialshareprivacy.css',
    'lang_path' : '/socialshareprivacy/lang/',
    'language'  : 'de'
});