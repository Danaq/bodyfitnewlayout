jqmenu = jQuery.noConflict();
jqmenu(document).ready(function() {
	jqmenu("#btn").click(function (a) {
		jqmenu(".main-nav").toggleClass("show");
		jqmenu(".subs").removeClass("selected");
		jqmenu(".subss").removeClass("selected");
		jqmenu(".subb").removeClass("selected");
		jqmenu(".dp1").removeClass("selected");
		jqmenu(".dp2").removeClass("selected");
		jqmenu(".dp3").removeClass("selected");
		jqmenu(".dp4").removeClass("selected");
		jqmenu(".dp5").removeClass("selected");// 2012 menü
	});

	jqmenu(".main-nav-ul  li .sub-arrow").click(function (e) { // erstes menü
		jqmenu(".subs").toggleClass("selected");
		jqmenu(".sub-arrow").toggleClass("rotate");
		jqmenu(".subb-arrow").removeClass("rotate");
		jqmenu(".sub-sub-arrow").removeClass("rotatesub");
		jqmenu(".erster-arrow").removeClass("rotate");
		jqmenu(".zweiter-arrow").removeClass("rotate");
		jqmenu(".dritter-arrow").removeClass("rotate");
		jqmenu(".vierter-arrow").removeClass("rotate");
		jqmenu(".fünfter-arrow").removeClass("rotate");
		jqmenu(".subss").removeClass("selected");
		jqmenu(".subb").removeClass("selected");
		jqmenu(".dp1").removeClass("selected");
		jqmenu(".dp2").removeClass("selected");
		jqmenu(".dp3").removeClass("selected");
		jqmenu(".dp4").removeClass("selected");
		jqmenu(".dp5").removeClass("selected");
	});

	jqmenu(".main-nav-ul  li .subb-arrow").click(function (f) { // zweites menü
		jqmenu(".subss").toggleClass("selected");
		jqmenu(".subb-arrow").toggleClass("rotate");
		jqmenu(".sub-sub-arrow").removeClass("rotatesub");
		jqmenu(".sub-arrow").removeClass("rotate");
		jqmenu(".subs").removeClass("selected");
		jqmenu(".subb").removeClass("selected");
		jqmenu(".dp1").removeClass("selected");
		jqmenu(".dp2").removeClass("selected");
		jqmenu(".dp3").removeClass("selected");
		jqmenu(".dp4").removeClass("selected");
		jqmenu(".dp5").removeClass("selected");
	});

	jqmenu(".main-nav-ul  li .sub-sub-arrow").click(function(g) { // drittes menü
		jqmenu(".subb").toggleClass("selected");
		jqmenu(".sub-sub-arrow").toggleClass("rotatesub");
		jqmenu(".erster-arrow").removeClass("rotate");
		jqmenu(".zweiter-arrow").removeClass("rotate");
		jqmenu(".dritter-arrow").removeClass("rotate");
		jqmenu(".vierter-arrow").removeClass("rotate");
		jqmenu(".fünfter-arrow").removeClass("rotate");
		jqmenu(".dp1").removeClass("selected");
		jqmenu(".dp2").removeClass("selected");
		jqmenu(".dp3").removeClass("selected");
		jqmenu(".dp4").removeClass("selected");
		jqmenu(".dp5").removeClass("selected");
	});

	jqmenu(".main-nav-ul  li ul .erster-arrow").click(function (h) { // 2016 menü
		jqmenu(".dp1").toggleClass("selected");
		jqmenu(".erster-arrow").toggleClass("rotate");
		jqmenu(".zweiter-arrow").removeClass("rotate");
		jqmenu(".dritter-arrow").removeClass("rotate");
		jqmenu(".vierter-arrow").removeClass("rotate");
		jqmenu(".fünfter-arrow").removeClass("rotate");
		jqmenu(".dp2").removeClass("selected");
		jqmenu(".dp3").removeClass("selected");
		jqmenu(".dp4").removeClass("selected");
		jqmenu(".dp5").removeClass("selected");
	});

	jqmenu(".main-nav-ul  li ul .zweiter-arrow").click(function (i) { // 2013 menü
		jqmenu(".dp2").toggleClass("selected");
		jqmenu(".zweiter-arrow").toggleClass("rotate");
		jqmenu(".erster-arrow").removeClass("rotate");
		jqmenu(".dritter-arrow").removeClass("rotate");
		jqmenu(".vierter-arrow").removeClass("rotate");
		jqmenu(".fünfter-arrow").removeClass("rotate");
		jqmenu(".dp1").removeClass("selected");
		jqmenu(".dp3").removeClass("selected");
		jqmenu(".dp4").removeClass("selected");
		jqmenu(".dp5").removeClass("selected");
	});

	jqmenu(".main-nav-ul  li ul .dritter-arrow").click(function (j) { // 2012 menü
		jqmenu(".dp3").toggleClass("selected");
		jqmenu(".dritter-arrow").toggleClass("rotate");
		jqmenu(".erster-arrow").removeClass("rotate");
		jqmenu(".zweiter-arrow").removeClass("rotate");
		jqmenu(".vierter-arrow").removeClass("rotate");
		jqmenu(".fünfter-arrow").removeClass("rotate");
		jqmenu(".dp1").removeClass("selected");
		jqmenu(".dp2").removeClass("selected");
		jqmenu(".dp4").removeClass("selected");
		jqmenu(".dp5").removeClass("selected");
	});

	jqmenu(".main-nav-ul  li ul .vierter-arrow").click(function (k) { // 2011 menü
		jqmenu(".dp4").toggleClass("selected");
		jqmenu(".vierter-arrow").toggleClass("rotate");
		jqmenu(".erster-arrow").removeClass("rotate");
		jqmenu(".zweiter-arrow").removeClass("rotate");
		jqmenu(".dritter-arrow").removeClass("rotate");
		jqmenu(".fünfter-arrow").removeClass("rotate");
		jqmenu(".dp1").removeClass("selected");
		jqmenu(".dp2").removeClass("selected");
		jqmenu(".dp3").removeClass("selected");
		jqmenu(".dp5").removeClass("selected");
	});

	jqmenu(".main-nav-ul  li ul .fünfter-arrow").click(function (l) { // 2009 menü
		jqmenu(".dp5").toggleClass("selected");
		jqmenu(".fünfter-arrow").toggleClass("rotate");
		jqmenu(".erster-arrow").removeClass("rotate");
		jqmenu(".zweiter-arrow").removeClass("rotate");
		jqmenu(".dritter-arrow").removeClass("rotate");
		jqmenu(".vierter-arrow").removeClass("rotate");
		jqmenu(".dp1").removeClass("selected");
		jqmenu(".dp2").removeClass("selected");
		jqmenu(".dp3").removeClass("selected");
		jqmenu(".dp4").removeClass("selected");
	});
});