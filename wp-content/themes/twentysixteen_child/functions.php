<?php

/* functions.php for childtheme of "Twentysixteen" "BodyfitCustomTemplate" */

function twentysixteen_child_styles() {
	wp_deregister_style( 'twentysixteen-style');
	wp_register_style('twentysixteen-style', get_template_directory_uri(). '/style.css');
	wp_enqueue_style('twentysixteen-style', get_template_directory_uri(). '/style.css');

	wp_enqueue_style( 'grid-style', get_stylesheet_directory_uri().'/style_darkGrid.css', array('twentysixteen-style') );
	wp_enqueue_style( 'customMenu-style', get_stylesheet_directory_uri().'/styleCustomMenu.css');

}
add_action( 'wp_enqueue_scripts', 'twentysixteen_child_styles' );

function twentysixteen_child_scripts() {
	wp_enqueue_script( 'twentysixteen-cookie-message', '/wp-content/themes/twentysixteen_child/js/jquery-cookiesdirective.js', array(), '', true );
	wp_enqueue_script( 'twentysixteen-FB-2click-like', '/jquery.socialshareprivacy/jquery.socialshareprivacy.js', array(), '', true );
	wp_enqueue_script( 'twentysixteen-cFB-like', '/wp-content/themes/twentysixteen_child/js/FB-like.js', array(), '', true );
	wp_enqueue_script( 'CustomMenuScript', '/wp-content/themes/twentysixteen_child/js/customMenu.js', array(), '', true );
	wp_enqueue_script( 'scrollMenu', '/wp-content/themes/twentysixteen_child/js/scrollMenu.js', array('jquery'), '', true );

}
add_action( 'wp_enqueue_scripts', 'twentysixteen_child_scripts' );

function register_footer_menu() {
  register_nav_menu('new-menu',__( 'Footer-Nav' ));
}
add_action( 'init', 'register_footer_menu' );

function filter_ptags_on_images($content){
   return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}
add_filter('the_content', 'filter_ptags_on_images');

add_action('get_header', 'remove_admin_login_header');
function remove_admin_login_header() {
	remove_action('wp_head', '_admin_bar_bump_cb');
}

//add_image_size( 'large_size_w', 600 );

function parent_redirect() {
  $excludeParentPages = ''; // e.g. '13, 42'
  $parentPagesObjs = get_pages(
    array('parent' => 0, 
          'exclude' => $excludeParentPages)
  );
  $parentPages = array();
  
  foreach ($parentPagesObjs as $k => $v) {
    $parentPages[] = $v->ID;
  }

  if (in_array($GLOBALS['post']->ID, $parentPages)) {
    $firstChildPage = get_pages(
      array('parent' => $GLOBALS['post']->ID, 
            'number' => 1, 
            'sort_column' => 'menu_order', 
            'sort_order' => 'asc')
    );
    
    if (isset($firstChildPage[0]->ID)) {
      wp_redirect(get_permalink($firstChildPage[0]->ID));
    }
  }
}
add_action('get_header', 'parent_redirect');

// NEW Posttype

function post_type_homepage(){

	register_post_type(
		'startseite',
		array(
			'labels' => array(
			 'name' =>'Startseite',
			 'singular_name' => 'Hauptereich',
			 'add_new_item'  => 'Neues Gridelement anlegen',
			),
			'public' => true,
			'show_ui'=> true,
			'supports'=> array(
				'title','excerpt', 'kategory', 'editor','thumbnail','revisions'
			)

		)
	);
}
add_action('init', 'post_type_homepage', 0);

// NEW Posttype

function post_type_men(){

	register_post_type(
		'Männer',
		array(
			'labels' => array(
			 'name' =>'Männerdisziplinen',
			 'singular_name' => 'Männerdisziplin',
			 'add_new_item'  => 'Neue Disziplin anlegen',
			),
			'public' => true,
			'show_ui'=> true,
			'supports'=> array(
				'title','excerpt','editor','thumbnail', 'revisions'
			)
		)
	);
}
add_action('init','post_type_men' ,0);

// NEW Posttype

function post_type_women(){

	register_post_type(
		'Frauen',
		array(
			'labels' => array(
			 'name' =>'Frauendisziplinen',
			 'singular_name' => 'Frauendisziplin',
			 'add_new_item'  => 'Neue Disziplin anlegen',
			),
			'public' => true,
			'show_ui'=> true,
			'supports'=> array(
				'title','excerpt','editor','thumbnail','revisions'
			)
		)
	);
}
add_action('init','post_type_women' ,0);


//class themeslug_walker_nav_menu extends Walker_Nav_Menu {
//  
//// add classes to ul sub-menus
//function start_lvl( &$output, $depth ) {
//    // depth dependent classes
//    $indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent
//    $display_depth = ( $depth + 1); // because it counts the first submenu as 0
//    $classes = array(
//        'sub-menu',
//        ( $display_depth % 2  ? 'menu-odd' : 'menu-even' ),
//        ( $display_depth >=2 ? 'sub-sub-menu' : '' ),
//        'menu-depth-' . $display_depth
//        );
//    $class_names = implode( ' ', $classes );
//  
//    // build html
//    //$output .= "\n" . $indent . '<div class="sub-menu"><ul class="' . $class_names . '">' . "\n";
//      $output .= "\n" . $indent . '<div class="sub-menu"><div class="als-container"><ul class="als-wrapper">' . "\n";
//
//
//}
//  
//// add main/sub classes to li's and links
// function start_el( &$output, $item, $depth, $args ) {
//    global $wp_query;
//    $indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent
//  
//    // depth dependent classes
//    $depth_classes = array(
//        ( $depth == 0 ? 'main-menu-item' : 'sub-menu-item' ),
//        ( $depth >=2 ? 'sub-sub-menu-item' : '' ),
//        ( $depth % 2 ? 'menu-item-odd' : 'menu-item-even' ),
//        'menu-item-depth-' . $depth
//    );
//
//    $depth_class_names = esc_attr( implode( ' ', $depth_classes ) );
//  
//    // passed classes
//    $classes = empty( $item->classes ) ? array() : (array) $item->classes;
//    $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );
//  
//    // build html
//    //$output .= $indent . '<li id="nav-menu-item-'. $item->ID . '" class="' . $depth_class_names . ' ' . $class_names . '">';
//    $output .= $indent . '<li class="als-item ' . $class_names . '">';
//
//    // link attributes
//    $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
//    $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
//    $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
//    $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
//    //$attributes .= ' class="menu-link ' . ( $depth > 0 ? 'sub-menu-link' : 'main-menu-link' ) . '"';
//    $attributes .= '';
//  
//    $item_output = sprintf( '%1$s<a%2$s>%3$s%4$s%5$s</a>%6$s',
//        $args->before,
//        $attributes,
//        $args->link_before,
//        apply_filters( 'the_title', $item->title, $item->ID ),
//        $args->link_after,
//        $args->after
//    );
//  
//    // build html
//    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
//
//} 
//}